package pendulum;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class RungeKuttaController
{
    @FXML
    private LineChart<Number, Number> YTChart;
    @FXML
    private LineChart<Number, Number> ZTChart;
    @FXML
    private LineChart<Number, Number> ZYChart;

    //region Y(t)
    @FXML
    private NumberAxis tAxis = new NumberAxis();
    @FXML
    private NumberAxis yAxis = new NumberAxis();
    private XYChart.Series<Number, Number> YTSeries;
    //endregion

    //region Z(t)
    @FXML
    private NumberAxis tAxis1 = new NumberAxis();
    @FXML
    private NumberAxis zAxis1 = new NumberAxis();
    private XYChart.Series<Number, Number> ZTSeries;
    //endregion

    //region Z(y)
    @FXML
    private NumberAxis yAxis2 = new NumberAxis();
    @FXML
    private NumberAxis zAxis2 = new NumberAxis();
    private XYChart.Series<Number, Number> ZYSeries;
    //endregion

    //region Parameters
    @FXML
    private TextField t0TextField;
    @FXML
    private TextField tMaxTextField;
    @FXML
    private TextField hTextField;
    @FXML
    private TextField Y0TextField;
    @FXML
    private TextField Z0TextField;


    @FXML
    private TextField epsilonTextField;
    @FXML
    private TextField omegaTextField;
    //endregion

    private static double epsilon;
    private static double omega;

    @FXML
    public void initialize()
    {
        yAxis.setLabel("y");
        tAxis.setLabel("t");
        YTSeries = new XYChart.Series<Number, Number>();
        YTSeries.setName("Y(t)");
        YTChart.getData().add(YTSeries);
        YTChart.setCreateSymbols(false);

        zAxis1.setLabel("z");
        tAxis1.setLabel("t");
        ZTSeries = new XYChart.Series<Number, Number>();
        ZTSeries.setName("Z(t)");
        ZTChart.getData().add(ZTSeries);
        ZTChart.setCreateSymbols(false);

        zAxis2.setLabel("z");
        yAxis2.setLabel("y");
        ZYSeries = new XYChart.Series<Number, Number>();
        ZYSeries.setName("Z(y)");
        ZYChart.getData().add(ZYSeries);
        ZYChart.setCreateSymbols(false);
    }

    private List<Pair<Number,Number>> YtList = new ArrayList<Pair<Number, Number>>();
    private List<Pair<Number,Number>> ZtList = new ArrayList<Pair<Number, Number>>();
    private List<Pair<Number,Number>> ZyList = new ArrayList<Pair<Number, Number>>();


    @FXML
    public void start()
    {
        YTSeries.getData().clear();
        ZTSeries.getData().clear();
        ZYSeries.getData().clear();
        int k = 2;
        double t, tMax, Yo, Y1=0, Zo, Z1=0, X1;
        double k1, k2, k4, k3, h;
        double q1, q2, q4, q3;
            /*
             *Начальные условия
             */
        t = new Double(t0TextField.getText());
        tMax = new Double(tMaxTextField.getText());
        Yo = new Double(Y0TextField.getText());
        Zo = new Double(Z0TextField.getText());

        h = new Double(hTextField.getText()); // шаг

        epsilon = new Double(epsilonTextField.getText());
        omega = new Double(omegaTextField.getText());

        System.out.println("\tt\t\tY\t\tZ");
        for (int i =0; t <= tMax; t += h)
        {

            k1 = h * f1(t, Yo, Zo);
            q1 = h * f2(t, Yo, Zo);

                /*k2 = h * f1(t + h/2.0, Yo + q1/2.0, Zo + k1/2.0);
                q2 = h * f2(t + h/2.0, Yo + q1/2.0, Zo + k1/2.0);

                k3 = h * f1(t + h/2.0, Yo + q2/2.0, Zo + k2/2.0);
                q3 = h * f2(t + h/2.0, Yo + q2/2.0, Zo + k2/2.0);

                k4 = h * f1(t + h, Yo + q3, Zo + k3);
                q4 = h * f2(t + h, Yo + q3, Zo + k3);*/

            //Z1 = Zo + (k1); //+ 2.0*k2 + 2.0*k3 + k4)/6.0;
            Y1 = Yo + k1;
            Z1 = Zo + q1;
            //Y1 = Yo + (q1);// + 2.0*q2 + 2.0*q3 + q4)/6.0;
            System.out.println("\t" + t + "     " + Y1 + "       " + Z1);// r(t + h, k) + "\t\t" + r(Y1 ,k) + "\t\t" + r(Z1 ,k));

            YtList.add(new Pair<Number, Number>(t,Y1));
            ZtList.add(new Pair<Number, Number>(t,Z1));
            ZyList.add(new Pair<Number, Number>(Y1,Y1));
            Yo = Y1;
            Zo = Z1;
            i++;
        }

        for (int i =0; i < YtList.size() ; i++)
        {
            YTSeries.getData().add(new XYChart.Data<Number, Number>(YtList.get(i).getKey(),YtList.get(i).getValue()));
            ZTSeries.getData().add(new XYChart.Data<Number, Number>(ZtList.get(i).getKey(),ZtList.get(i).getValue()));
            ZYSeries.getData().add(new XYChart.Data<Number, Number>(ZyList.get(i).getKey(),ZyList.get(i).getValue()));
        }
    }

    /**
     * dy/dt
     */
    public static double f1(double x, double y, double z)
    {
        return z;
    }

    /**
     * dz/dt
     */
    public static double f2(double x, double y, double z)
    {
        return -(omega * omega * y) + (epsilon * z);
    }
}
