package diplom;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.List;

public class RungeKuttaController {
    @FXML
    private LineChart<Number, Number> LTChart;
    @FXML
    private LineChart<Number, Number> FuTChart;
    @FXML
    private LineChart<Number, Number> Alpha1TChart;
    @FXML
    private Label graphLabel;
    @FXML
    private Slider graphSlider;

    //region Y(t)
    @FXML
    private NumberAxis tAxis = new NumberAxis();
    @FXML
    private NumberAxis LAxis = new NumberAxis();
    private XYChart.Series<Number, Number> LTSeries;
    //endregion

    //region Z(t)
    @FXML
    private NumberAxis tAxis1 = new NumberAxis();
    @FXML
    private NumberAxis FuAxis1 = new NumberAxis();
    private XYChart.Series<Number, Number> FuTSeries;
    //endregion

    //region Z(y)
    @FXML
    private NumberAxis yAxis2 = new NumberAxis();
    @FXML
    private NumberAxis alpha1Axis2 = new NumberAxis();
    private XYChart.Series<Number, Number> Alpha1TSeries;
    //endregion

    //region Parameters
    @FXML
    private TextField t0TextField;
    @FXML
    private TextField tMaxTextField;
    @FXML
    private TextField hTextField;
    @FXML
    private TextField Y0TextField;
    @FXML
    private TextField Z0TextField;


    @FXML
    private TextField epsilonTextField;
    @FXML
    private TextField omegaTextField;
    //endregion


    @FXML
    public void initialize() {
        LAxis.setLabel("L");
        tAxis.setLabel("t");
        LTSeries = new XYChart.Series<Number, Number>();
        LTSeries.setName("L(t)");
        LTChart.getData().add(LTSeries);
        LTChart.setCreateSymbols(false);

        FuAxis1.setLabel("Fu");
        tAxis1.setLabel("t");
        FuTSeries = new XYChart.Series<Number, Number>();
        FuTSeries.setName("Fu(t)");
        FuTChart.getData().add(FuTSeries);
        FuTChart.setCreateSymbols(false);

        alpha1Axis2.setLabel("alpha 1");
        yAxis2.setLabel("t");
        Alpha1TSeries = new XYChart.Series<Number, Number>();
        Alpha1TSeries.setName("Alpha 1(t)");
        Alpha1TChart.getData().add(Alpha1TSeries);
        Alpha1TChart.setCreateSymbols(false);
    }

    private List<Double> FuList = new ArrayList<Double>();
    private List<Double> KvList = new ArrayList<Double>();

    private List<Double> VLList = new ArrayList<Double>();
    private List<Double> LList = new ArrayList<Double>();
    private List<Double> omegaTetaList = new ArrayList<Double>();
    private List<Double> tetaList = new ArrayList<Double>();
    private List<Double> omegaZ1List = new ArrayList<Double>();
    private List<Double> alpha1List = new ArrayList<Double>();
    private List<Double> omegaZ2List = new ArrayList<Double>();
    private List<Double> alpha2List = new ArrayList<Double>();
    private List<Double> tList = new ArrayList<Double>();

//    double newL, newLEnd, m1, m2, newVl, newkv1, newkv2, Lk1, Lk2, newFu, newKv, Me, newt;
//    double newteta;
//    double newomegaH;
//    double newomegaTeta;
//    double newomegaZ1, newomegaZ2, newalpha1 = 0, newalpha2 = 0;

    double t, tMax, Yo = 0, Y1 = 0, Zo, Z1 = 0, X1;
    double k1, k2, k4, k3, h;
    double q1, q2, q4, q3;

    double L, LEnd, m1, m2, Vl, kv1, kv2, Lk1, Lk2, Fu, Kv, Me;
    double teta;
    double omegaH;
    double omegaTeta;
    double omegaZ1, omegaZ2, alpha1 = 0, alpha2 = 0;


    public void additionalInit(){
        graphSlider.valueProperty().addListener(el -> graphLabel.setText(String.valueOf(graphSlider.getValue())));
    }

    @FXML
    public void start() {
        LTSeries.getData().clear();
        FuTSeries.getData().clear();
        Alpha1TSeries.getData().clear();

        int k = 2;

        double f1, f2, f3, f4, f5, f6, f7, f8;

        /*
         *Начальные условия
         */
        //        t = new Double(t0TextField.getText());
        //        tMax = new Double(tMaxTextField.getText());
        //        Yo = new Double(Y0TextField.getText());
        //        Zo = new Double(Z0TextField.getText());

        m1 = 2;
        m2 = 2;
        Vl = 0.5;
        L = 0.1;
        LEnd = 3000;
        h = 0.01; // шаг
        t = 0;//время
        teta = 0;
        omegaH = 0.0011;
        omegaTeta = 0;


        //параметры коэфф пропорциональности
        kv1 = 0.05;
        kv2 = 3;// подбирается так, что бы Vl ~ 0

        Lk1 = 0.1; //длина ребра спутника 1
        Lk2 = 0.1; // длина ребра спутника 2

        omegaZ1 = 0.05; //угловая скорость нс 1
        omegaZ2 = -0.05; // угловая скорость нс 2

        alpha1=0;
        alpha2 =0;

        Fu = Fu(Kv, Vl);

        Kv = Kv(kv1, kv2, L, LEnd);
        //        teta = new Double(epsilonTextField.getText());
        //        omegah = new Double(omegaTextField.getText());

        //VLList.add(Vl);


       // double Dteta = 0;

        addValuesToLists();

        System.out.println("Me = " + Me(m1, m2));
        System.out.println("\tt\t\tL\t\tFu\t\tKv\t\tVL\t\tomegaTeta\t\tteta\t\tomegaZ1\t\talpha1\t\tomegaZ2\t\talpha2");
        System.out.println(t + "\t\t" + L + "\t\t" + Fu + "\t\t" + Kv + "\t\t" + Vl + "\t\t" + omegaTeta + "\t\t" + teta + "\t\t" + omegaZ1 + "\t\t" + alpha1 + "\t\t" + omegaZ2 + "\t\t" + alpha2);
        while (L <= LEnd && t < 100) {

            Me = Me(m1, m2);

            Kv = Kv(kv1, kv2, L, LEnd);
            Fu = Fu(Kv, Vl);

            //Vl
            f1 = L * (omegaTeta + omegaH) * (omegaTeta + omegaH) + omegaH * omegaH * (3 * Math.cos(teta) - 1) - Fu / Me;
            //L
            f2 = Vl;
            //OmegaTeta
            f3 = -2 * Vl / L * (omegaTeta + omegaH) - 3d / 2d * omegaH * omegaH * Math.sin(2 * teta);
            //teta
            f4 = omegaTeta;
            //omegaZ1
            f5 = (-Fu * Lk1 / 2 * Math.sin(alpha1)) / (m1 * Lk1 * Lk1 / 6);
            //alpha1
            f6 = omegaZ1 - omegaTeta;
            //omegaZ2
            f7 = (-Fu * Lk2 / 2 * Math.sin(alpha2)) / (m2 * Lk2 * Lk2 / 6);
            //alpha2
            f8 = omegaZ2 - omegaTeta;


            Vl = getNextEuler(Vl, h, f1);
            L = getNextEuler(L, h, f2);
            omegaTeta = getNextEuler(omegaTeta, h, f3);
            teta = getNextEuler(teta, h, f4);
            omegaZ1 = getNextEuler(omegaZ1, h, f5);
            alpha1 = getNextEuler(alpha1, h, f6);
            omegaZ2 = getNextEuler(omegaZ2, h, f7);
            alpha2 = getNextEuler(alpha2, h, f8);
            t += h;

            //вывод на экран
            System.out.println(t + "\t\t" + L + "\t\t" + Fu + "\t\t" + Kv + "\t\t" + Vl + "\t\t" + omegaTeta + "\t\t" + teta + "\t\t" + omegaZ1 + "\t\t" + alpha1 + "\t\t" + omegaZ2 + "\t\t" + alpha2);

            //Обновление листов
            addValuesToLists();
        }
        updateGraph();
    }

    /**
     * @param y0 начальное условие
     * @param h  шаг
     * @param Fx значение функции
     */
    private double getNextEuler(double y0, double h, double Fx) {
        return y0 + (h * Fx);
    }


    private void addValuesToLists() {
        FuList.add(Fu);
        KvList.add(Kv);
        VLList.add(Vl);
        LList.add(L);
        omegaTetaList.add(omegaTeta);
        tetaList.add(teta);
        omegaZ1List.add(omegaZ1);
        alpha1List.add(alpha1);
        omegaZ2List.add(omegaZ2);
        alpha2List.add(alpha2);
        tList.add(t);
    }

    private void updateGraph(){
        for (int i = 0; i < tList.size();i++) {
            if (i % graphSlider.getValue() == 0) {
                LTSeries.getData().add(new XYChart.Data<Number, Number>(tList.get(i), LList.get(i)));
                FuTSeries.getData().add(new XYChart.Data<Number, Number>(tList.get(i), FuList.get(i)));
                Alpha1TSeries.getData().add(new XYChart.Data<Number, Number>(tList.get(i), alpha1List.get(i)));
            }
        }
    }
    //region deprecated
    /**
     * Vl / dt
     */
    private static double VlDt(double L, double epsilon, double epsilonDt, double omegaH, double Fu, double Me) {
        return L * (epsilonDt + omegaH) * (epsilonDt + omegaH) + omegaH * omegaH * (3 * Math.cos(epsilon) - 1) - Fu / Me;
    }

    /**
     * L / dt
     */
    private static double LDt(double Vl) {
        return Vl;
    }

    /**
     * omegaEps / dt
     */
    private static double omegaTetaDt(double Vl, double L, double epsilonDt, double epsilon, double omegaH) {
        return -2 * Vl / L * (epsilonDt + omegaH) - 3d / 2d * omegaH * omegaH * Math.sin(2 * epsilon);
    }

    /**
     * epsilon / dt
     */
    private static double tetaDt(double omegaEps) {
        return omegaEps;
    }
    //endregion


    /**
     * Приведенная масса
     *
     * @param m1 масса 1 спутника
     * @param m2 масса 2 спутника
     */
    private static double Me(double m1, double m2) {
        return (m1 * m2) / (m1 + m2);
    }


    /**
     * Сила натяжения тросса (управляющая сила)
     *
     * @param Kv коэффициент пропорциональности
     * @param Vl скорость выпуска троса из механизма управления
     */
    private static double Fu(double Kv, double Vl) {
        return Kv * Vl;
    }


    private static double L1 = -1;

    /**
     * Коэффициент пропорциональности
     *
     * @param kv1  параметр
     * @param kv2  параметр (регулируется, что бы Vl ~ 0)
     * @param L    текущая длина троса
     * @param LEnd максимальная длина троса
     */
    private static double Kv(double kv1, double kv2, double L, double LEnd) {
        if (L1 == -1) {
            L1 = 0.9 * LEnd;
        }
        if (L <= L1) {
            return kv1;
        }
        if (L1 < L && L < LEnd) {
            return kv1 + ((L - L1) / (LEnd - L1) * (kv2 - kv1));
        }
        return -1;
    }


    //region deprecated
    /**
     * Угловая скорость 1 спутника
     *
     * @param mZ1 момент силы натяжения троса
     * @param Jz1 момент инерции 1 НС
     */
    private static double omegaZ1(double mZ1, double Jz1) {
        return mZ1 / Jz1;
    }

    /**
     * Угол 1 НС
     *
     * @param omegaZ1  угловая скорость 1 спутника
     * @param omegaEps
     */
    private static double alphaK1(double omegaZ1, double omegaEps) {
        return omegaZ1 - omegaEps;
    }

    /**
     * Угловая скорость 2 спутника
     *
     * @param mZ2 момент силы натяжения троса
     * @param Jz2 момент инерции 2 НС
     */
    private static double omegaZ2(double mZ2, double Jz2) {
        return mZ2 / Jz2;
    }

    /**
     * Угол 2 НС
     *
     * @param omegaZ2  угловая скорость 1 спутника
     * @param omegaEps
     */
    private static double alphaK2(double omegaZ2, double omegaEps) {
        return omegaZ2 - omegaEps;
    }


    /**
     * Момент инерции НС
     *
     * @param m  масса
     * @param Lk длина ребра кубических НС
     */
    private static double Jz(double m, double Lk) {
        return m * Lk * Lk / 6;
    }

    /**
     * Моменты от силы натяжения троса
     *
     * @param Fu    Сила натяжения тросса
     * @param Lk    длина ребра кубических НС
     * @param alpha угл между осью НС и тросом
     */
    private static double Mz(double Fu, double Lk, double alpha) {
        return -Fu * Lk / 2 * Math.sin(alpha);
    }
    //endregion

}