package diplom;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application
{

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("RungeKutta.fxml"));
        Parent root = loader.load();
        stage.setTitle("RungeKutta");
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(getClass().getResource("../resources/style.css").toString());
        stage.setScene(scene);
        RungeKuttaController controller = loader.getController();
        controller.additionalInit();
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
